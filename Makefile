CXX = g++-11 -std=c++20
LDFLAGS = -lfmt


.PHONY: all

all: bin \
	 bin/hello_world \
	 bin/is_multiple_of \
	 bin/genfunc \
	 bin/lambda \
	 bin/strucbind \

bin:
	mkdir -p bin

bin/is_multiple_of: concepts/is_multiple_of.cpp
	$(CXX) $^ $(LDFLAGS) -o $@

bin/genfunc: genfunc/genfunc.cpp
	$(CXX) $^ $(LDFLAGS) -o $@

bin/lambda: genfunc/lambda.cpp
	$(CXX) $^ $(LDFLAGS) -o $@

bin/strucbind: strucbind/strucbind.cpp
	$(CXX) $^ $(LDFLAGS) -o $@

bin/hello_world: hello_world/hello.cpp hello_world/world.cpp hello_world/main.cpp
	$(CXX) -fmodules-ts $^ $(LDFLAGS) -o $@
