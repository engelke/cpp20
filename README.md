# cpp20

A short and by no means complete walk through the features introduced with C++20 and previous versions that changed the language drastically.

## Generic Lambda and Named Functions
Generic lambda functions have been part of the standard since C++11. Now, C++20 extends this concept to named function removing the need to declare such generic functions as templates.

Examples:
* [Generic named functions](genfunc/genfunc.cpp)
* [Generic lambda functions](genfunc/lambda.cpp)

## Concepts
Concepts are an entirely new feature, that makes workarounds such as SFINAE obsolete. Concepts allow us to impose conditions on the types that are accepted as template arguments or arguments of generic functions. This simplifies and shortens the work to specialize function and class templates. Probably the most significant change introduced with C++20.

Example:
* [is_multiple_of](concepts/is_multiple_of.cpp)

## Modules
Traditionally, dating back to its origins in C, C++ split declaration and definition of functions into headers (.hpp) and implementation (.cpp) files, respectively. This made sense back in the days, when all functions were defined in the .c files. But ever since templates existed, this order got messed up. Templates musst be defined in the header, except they are full specialisations. Then, they either must be declared `inline` or go in the .cpp file. In the end one part of the code is in the header and the other one is in the .cpp file. C++20 introduces modules, where both templates and functions can happily coexist.

Example:
* [Modules](hello_world)

## Structured Bindings (C++17)
A bit of syntactical sugar further reducing the bloat that comes with returning multiple objects from a function.

Example:
* [Structured Bindings](strucbind/strucbind.cpp)
