#include <fmt/core.h>
#include <type_traits>
#include <string>
#include <cmath>

template <typename T1, typename T2>
concept ModuloDivisible = requires(T1 a, T2 b) { a % b; };

template <typename T1, typename T2>
concept FmodDivisible = requires(T1 a, T2 b) { fmod(a, b); };

template <typename T1, typename T2>
requires ModuloDivisible<T1, T2>
bool is_multiple_of(const T1& a, const T2& b) { return a % b == 0; }

template <typename T1, typename T2>
requires (FmodDivisible<T1, T2> && !ModuloDivisible<T1, T2>)
bool is_multiple_of(const T1& a, const T2& b) { return fmod(a, b) < 1e-13; }


int main()
{
    fmt::print("{} {} {}\n", 8, 2, is_multiple_of(8, 2));
    fmt::print("{} {} {}\n", 8, 3, is_multiple_of(8, 3));
    fmt::print("{} {} {}\n", 6.28, 3.14, is_multiple_of(6.28, 3.14));
    fmt::print("{} {} {}\n", 6.28, 3.15, is_multiple_of(6.28, 3.15));
}
