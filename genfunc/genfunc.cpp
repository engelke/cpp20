#include <fmt/core.h>

/* Until C++17 */
template <typename T>
void print_old(const T& a)
{
    fmt::print("{}\n", a);
}

/* Now with C++20, named functions can be generic like lambdas */
void print(const auto& a) { fmt::print("Value {}\n", a); }

/* We can even use concepts to specialize the function */
void print(const std::integral auto& a) { fmt::print("Integer {}\n", a); }
void print(const std::floating_point auto& a) { fmt::print("Floating Point {}\n", a); }

/* This also works for member functions */
struct Foo
{
    void print(const auto& a) { fmt::print("Foo: Value {}\n", a); }
    void print(const std::integral auto& a) { fmt::print("Foo: Integer {}\n", a); }
};

int main()
{
    print("abc");
    print(1);
    print(3.14);

    Foo foo;
    foo.print("abc");
    foo.print(3);
}
