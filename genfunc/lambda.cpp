#include <algorithm>
#include <fmt/ranges.h>
#include <numeric>
#include <vector>

int main()
{
    std::vector<int> v(10);
    std::iota(std::begin(v), std::end(v), 0); // fill with 0 ... 9
    fmt::print("Before: {}\n", v);

    /* generic lambda as a comparison function */
    std::sort(std::begin(v), std::end(v),
              [](const auto& a, const auto& b) { return a > b; });

    fmt::print("Sorted: {}\n", v);

    /* generic lambda assigned to a object 'is_even' as a unary predicate */
    auto is_even = [](auto i) { return i % 2 == 0; };
    auto new_end = std::remove_if(std::begin(v), std::end(v), is_even);
    v.erase(new_end, std::end(v));

    fmt::print("Even deleted: {}\n", v);

    /* and with a capture */
    int j = 5;
    v.erase(std::remove_if(std::begin(v), std::end(v), [j](auto i) { return i == j; }),
            std::end(v));
    fmt::print("5 deleted: {}\n", v);
}
