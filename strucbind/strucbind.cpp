#include <tuple>
#include <vector>
#include <fmt/ranges.h>

/* A function returning a tuple */
auto get_ab() { return std::tuple(1, "abc"); }

int main()
{
    /* unpack tuple into a and b */
    auto [a, b] = get_ab();
    fmt::print("a={} b={}\n", a, b);

    /* prepare a vector of tuples */
    std::vector<std::tuple<int, double>> v;
    for (int i = 0; i < 10; ++i)
    {
        v.emplace_back(i, 0.1 * i);
    }

    /* use a range-based for loop to iterate v */
    for (auto [i, x] : v)
    {
        fmt::print("{:d} {:.1f}\n", i , x);
    }
}
